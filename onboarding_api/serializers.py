from rest_framework import serializers

from onboarding.models import OnboardingEntry


class OnboardingEntrySerializer(serializers.ModelSerializer):
    class Meta:
        model = OnboardingEntry
        fields = '__all__'
        read_only_fields = ['uuid', 'created_at', 'updated_at', 'deleted_at', 'is_deleted']
