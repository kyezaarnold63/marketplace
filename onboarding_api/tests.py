from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from onboarding.models import OnboardingEntry


class OnboardingEntryAPITest(APITestCase):

    def setUp(self):
        self.onboarding_entry = OnboardingEntry.objects.create(
            store_name='Test Store',
            balance_on_gift_card=100,
            balance_currency='USD',
            card_selling_price=90,
            card_price_currency='USD',
            crypto_network='ethereum',
            crypto_wallet_address='1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
            email='test_user@email.com'
        )

    def test_onboarding_entry_api_get(self):
        url = reverse('onboarding_api:list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_onboarding_entry_api_post(self):
        url = reverse('onboarding_api:create')
        data = {
            "store_name": "Test Store 001",
            "balance_on_gift_card": 300,
            "balance_currency": "USD",
            "card_selling_price": 20,
            "card_price_currency": "USD",
            "crypto_network": "polygon",
            "crypto_wallet_address": "1A1zP1eP5QGefi2DMasaTL5SLmv7DivfNa",
            "email": "test_user001@email.com"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

