from django.urls import path

from onboarding_api.views import OnboardingEntryList, OnboardingEntryCreate

app_name = 'onboarding_api'
urlpatterns = [
    path('seller_intake_survey/', OnboardingEntryCreate.as_view(), name='create'),
    path('seller_intake_survey_results/', OnboardingEntryList.as_view(), name='list'),

]
