from rest_framework import generics

from onboarding.models import OnboardingEntry
from onboarding_api.serializers import OnboardingEntrySerializer


class OnboardingEntryCreate(generics.CreateAPIView):
    queryset = OnboardingEntry.objects.all()
    serializer_class = OnboardingEntrySerializer


class OnboardingEntryList(generics.ListAPIView):
    queryset = OnboardingEntry.objects.all()
    serializer_class = OnboardingEntrySerializer
