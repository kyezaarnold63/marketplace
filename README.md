# Watermelon Markets Project
A Django project to manage watermelon markets Sellers survey intakes

## Important Pre-requisites
- [Docker](https://docs.docker.com/get-docker/)
- [Docker Compose](https://docs.docker.com/compose/install/)
- [Python 3.8](https://www.python.org/downloads/release/python-380/)
- [NodeJS](https://nodejs.org/en/download/)
- [mysqlclient](https://pypi.org/project/mysqlclient/)

## Development Setup

Clone the project and change directory to ``marketplace``
```
git clone https://gitlab.com/kyezaarnold63/marketplace.git

cd marketplace
```

Create a virtual environment and activate it
```
python3 -m venv venv
source venv/bin/activate
```

Install the project dependencies
```
pip install -r requirements.txt
```

Migrations
```
python manage.py migrate
```

Create a superuser
```
python manage.py createsuperuser
```

Run API server
```
python manage.py runserver
```

Frontend setup
```
cd frontend
npm install
```

Run frontend server
```
npm start
```

Run the project

- Go to http://localhost:3000/ to view the survey form
- Go to http://localhost:3000/results/ to view the survey results

## Docker Setup

Create a ``.env`` file in the ``marketplace`` directory
```
# copy sample.env to .env and edit accordingly
# note for DEBUG, values are 1 for True and 0 for False
```

Inside the ``marketplace`` directory build the docker containers for the project and start django server
```
docker-compose up -d --build
docker-compose exec api python manage.py migrate --noinput
docker-compose exec api python manage.py createsuperuser
docker-compose exec api python manage.py collectstatic --no-input --clear

```

## Running the project

Check the running containers
```
docker-compose ps
```

- Go to http://localhost/ to view the survey form
- Go to http://localhost/results/ to view the survey results


## Development Common commands

### Execute Django commands

### start docker services and django server
```
# start docker in foreground
docker-compose up

# start docker in background
docker-compose up -d
```

### follow docker django service logs
```
docker-compose logs -f web
```

### follow docker logs for any other service
```
docker-compose logs -f <service>
```

### execute commands inside docker django web service
```
docker-compose exec web bash
```

### execute commands inside any other docker service
```
docker-compose exec <service> bash
```

#### create new django app
```
docker-compose run web django-admin startapp core
```

### executing manage.py commands
```
docker-compose run web python manage.py <command>
```

### restart docker and all services
```
docker-compose restart
```

### shutdown docker and all services
```
docker-compose down
```