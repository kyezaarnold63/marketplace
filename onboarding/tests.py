from django.test import TestCase

from onboarding.models import OnboardingEntry


class OnboardingEntryModelTest(TestCase):

    def setUp(self):
        self.onboarding_entry = OnboardingEntry.objects.create(
            store_name='Test Store',
            balance_on_gift_card=100,
            balance_currency='USD',
            card_selling_price=90,
            card_price_currency='USD',
            crypto_network='ethereum',
            crypto_wallet_address='1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
            email='test_user@email.com'
        )

    def test_onboarding_entry_model(self):
        onboarding_entry = OnboardingEntry.objects.get(id=1)

        store_name = f'{onboarding_entry.store_name}'
        balance_on_gift_card = f'{onboarding_entry.balance_on_gift_card}'
        balance_currency = f'{onboarding_entry.balance_currency}'
        card_selling_price = f'{onboarding_entry.card_selling_price}'
        card_price_currency = f'{onboarding_entry.card_price_currency}'
        crypto_network = f'{onboarding_entry.crypto_network}'
        crypto_wallet_address = f'{onboarding_entry.crypto_wallet_address}'
        email = f'{onboarding_entry.email}'

        self.assertEqual(store_name, 'Test Store')
        self.assertEqual(balance_on_gift_card, '100.00')
        self.assertEqual(balance_currency, 'USD')
        self.assertEqual(card_selling_price, '90.00')
        self.assertEqual(card_price_currency, 'USD')
        self.assertEqual(crypto_network, 'ethereum')
        self.assertEqual(crypto_wallet_address, '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa')
        self.assertEqual(email, f'{onboarding_entry.email}')

    def test_onboarding_entry_model_str(self):
        onboarding_entry = OnboardingEntry.objects.get(id=1)

        store_name = f'{onboarding_entry.store_name}'

        self.assertEqual(str(onboarding_entry), store_name)

    def test_onboarding_entry_soft_delete(self):
        onboarding_entry = OnboardingEntry.objects.get(id=1)
        onboarding_entry.soft_delete()

        self.assertEqual(onboarding_entry.is_deleted, True)

