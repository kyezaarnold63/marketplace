from django.contrib import admin

from onboarding.models import OnboardingEntry


class OnboardingEntryAdmin(admin.ModelAdmin):
    list_display = ['uuid', 'store_name', 'balance_on_gift_card', 'balance_currency', 'card_selling_price',
                    'card_price_currency', 'crypto_network', 'crypto_wallet_address', 'email', 'created_at',
                    'updated_at', 'deleted_at', 'is_deleted']
    list_filter = ['created_at', 'updated_at', 'deleted_at', 'is_deleted']
    search_fields = ['store_name', 'email']

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ['uuid', 'created_at', 'updated_at', 'deleted_at', 'is_deleted']
        else:
            return []


models = [
    (OnboardingEntry, OnboardingEntryAdmin),
]

for model, model_admin in models:
    admin.site.register(model, model_admin)
