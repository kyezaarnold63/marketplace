# Generated by Django 4.2 on 2023-05-01 07:02

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='OnboardingEntry',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(db_index=True, default=uuid.uuid4, editable=False, unique=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created At')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated At')),
                ('deleted_at', models.DateTimeField(blank=True, null=True, verbose_name='Deleted At')),
                ('is_deleted', models.BooleanField(default=False, verbose_name='Is Deleted')),
                ('store_name', models.CharField(max_length=100, verbose_name='Store Name')),
                ('balance_on_gift_card', models.DecimalField(decimal_places=2, max_digits=10, verbose_name='Balance On Card')),
                ('balance_currency', models.CharField(max_length=10, verbose_name='Card Balance Currency')),
                ('card_selling_price', models.DecimalField(decimal_places=2, max_digits=10, verbose_name='Card Selling Price')),
                ('card_price_currency', models.CharField(max_length=10, verbose_name='Card Price Currency')),
                ('crypto_network', models.CharField(choices=[('polygon', 'Polygon'), ('ethereum', 'Ethereum')], max_length=10, verbose_name='Crypto Network')),
                ('crypto_wallet_address', models.CharField(max_length=100, verbose_name='Wallet Address')),
                ('email', models.EmailField(max_length=100, verbose_name='Email')),
            ],
            options={
                'verbose_name': 'Seller Onboarding Entry',
                'verbose_name_plural': 'Seller Onboarding Entries',
                'ordering': ['-created_at'],
            },
        ),
    ]
