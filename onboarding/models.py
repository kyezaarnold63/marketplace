import uuid as uuid
from django.db import models
from django.utils import timezone


class BaseModel(models.Model):

    class Meta:
        abstract = True

    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True, db_index=True)
    created_at = models.DateTimeField(verbose_name='Created At', auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name='Updated At', auto_now=True)
    deleted_at = models.DateTimeField(verbose_name='Deleted At', null=True, blank=True)
    is_deleted = models.BooleanField(verbose_name='Is Deleted', default=False)

    def soft_delete(self):
        self.deleted_at = timezone.now()
        self.is_deleted = True
        self.save()


class OnboardingEntry(BaseModel):

    # model custom manager
    class OnboardingEntryManager(models.Manager):
        def get_queryset(self):
            return super().get_queryset().filter(is_deleted=False)

    # crypto network choices
    class CryptoNetwork(models.TextChoices):
        POLYGON = 'polygon', 'Polygon'
        ETHEREUM = 'ethereum', 'Ethereum'

    store_name = models.CharField(verbose_name='Store Name', max_length=100)
    balance_on_gift_card = models.DecimalField(verbose_name='Balance On Card', max_digits=10, decimal_places=2)
    balance_currency = models.CharField(verbose_name='Card Balance Currency', max_length=10)
    card_selling_price = models.DecimalField(verbose_name='Card Selling Price', max_digits=10, decimal_places=2)
    card_price_currency = models.CharField(verbose_name='Card Price Currency', max_length=10)
    crypto_network = models.CharField(verbose_name='Crypto Network', choices=CryptoNetwork.choices, max_length=10)
    crypto_wallet_address = models.CharField(verbose_name='Wallet Address', max_length=100)
    email = models.EmailField(verbose_name='Email', max_length=100)

    # meta class
    class Meta:
        verbose_name = 'Seller Onboarding Entry'
        verbose_name_plural = 'Seller Onboarding Entries'
        ordering = ['-created_at']

    def __str__(self):
        return self.store_name
