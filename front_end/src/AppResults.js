import React from 'react';
import './App.css';

import Dashboard from "./components/DashBoard";

function AppResults() {
    return (
        <div className="App">
            <Dashboard/>
        </div>
    );
}

export default AppResults;
