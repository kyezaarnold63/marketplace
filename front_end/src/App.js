import React from 'react';
import './App.css';

import SellerOnboardingFormModal from './components/SellerOnboardingFormModal';

function App() {
  return (
    <div className="App">
      <SellerOnboardingFormModal />
    </div>
  );
}

export default App;
