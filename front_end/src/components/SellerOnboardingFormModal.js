import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Paper from '@mui/material/Paper';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import {createTheme, ThemeProvider} from '@mui/material/styles';


import Header from './Header';
import StoreNameForm from './StoreNameForm';
import BalanceForm from "./BalanceForm";
import SellingPriceForm from "./SellingPriceForm";
import CryptoNetworkForm from "./CryptoNetworkForm";
import WalletAddressForm from "./WalletAddressForm";
import EmailForm from "./EmailForm";


const steps = ['Store name', 'Balance', 'Selling price', 'Crypto network', 'Wallet address', 'Email'];

function getStepContent(step, handleFormChange, formData, error) {

    switch (step) {
        case 0:
            return <StoreNameForm handleFormChange={handleFormChange} formData={formData}
                                  error={error}/>;
        case 1:
            return <BalanceForm handleFormChange={handleFormChange} formData={formData}
                                error={error}/>;
        case 2:
            return <SellingPriceForm handleFormChange={handleFormChange} formData={formData}
                                     error={error}/>;
        case 3:
            return <CryptoNetworkForm handleFormChange={handleFormChange} formData={formData}
                                      error={error}/>;
        case 4:
            return <WalletAddressForm handleFormChange={handleFormChange} formData={formData}
                                      error={error}/>;
        case 5:
            return <EmailForm handleFormChange={handleFormChange} formData={formData}
                              error={error}/>;
        default:
            throw new Error('Unknown step');
    }
}

const theme = createTheme();

export default function SellerOnboardingFormModal() {
    const [activeStep, setActiveStep] = React.useState(0);

    const [formData, setFormData] = React.useState({
        store_name: "",
        balance_on_gift_card: null,
        balance_currency: "USD",
        card_selling_price: null,
        card_price_currency: "USD",
        crypto_network: 'polygon',
        crypto_wallet_address: "",
        email: ""
    });

    const [error, setError] = React.useState('')

    const handleFormChange = (fieldName, fieldValue) => {
        setFormData(prevFormData => ({
            ...prevFormData,
            [fieldName]: fieldValue
        }));
    };

    const isValidEmail = (email) => {
        // Regular expression for email validation
        let emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

        // Test the email against the pattern
        return emailPattern.test(email);
    }


    const handleNext = () => {
        // validate the form before proceeding
        let isValid = true;
        setError('');

        switch (activeStep) {
            case 0:
                if (!formData.store_name) {
                    setError('Please enter a store name!');
                    isValid = false;
                }
                break;
            case 1:
                if (!formData.balance_on_gift_card) {
                    setError('This field is required!');
                    isValid = false;
                } else if (isNaN(formData.balance_on_gift_card)) {
                    setError('Please enter a number!');
                    isValid = false;
                }
                break;
            case 2:
                if (!formData.card_selling_price) {
                    setError('This field is required!');
                    isValid = false;
                } else if (isNaN(formData.card_selling_price)) {
                    setError('Please enter a number!');
                    isValid = false;
                }
                break;
            case 4:
                if (!formData.crypto_wallet_address) {
                    setError('Please enter a wallet address!');
                    isValid = false;
                }
                break;
            case 5:
                if (!formData.email) {
                    setError('This field is required!');
                    isValid = false;
                } else if (!isValidEmail(formData.email)) {
                    setError('Please enter a valid email address!');
                    isValid = false;
                }
                break;
        }

        if (isValid) {
            if (activeStep === steps.length - 1) {
                // submit the form
                onSubmit(formData);
            }
            setActiveStep(activeStep + 1);
            setError('')
        }
    };

    const handleBack = () => {
        // this handles the back button
        setActiveStep(activeStep - 1);
    };

    const onSubmit = (data) => {

        const apiHost = !process.env.REACT_APP_HOST_IP_ADDRESS ? 'localhost' : process.env.REACT_APP_HOST_IP_ADDRESS;
        const onboardingEndpoint = `http://${apiHost}/backend/api/seller_intake_survey/`;
        fetch(onboardingEndpoint, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
        })
            .then((response) => response.json())
            .then((response) => {
                console.log("Success:", response);
                setActiveStep(activeStep + 1);
            })
            .catch((error) => {
                console.error("Error:", error);
            });
    };

    return (
        <ThemeProvider theme={theme}>
            <CssBaseline/>

            {/* Header */}
            <Header/>

            {/* Form model */}
            <Container component="main" maxWidth="lg" sx={{mb: 4}}>
                <Paper variant="outlined" sx={{my: {xs: 3, md: 6}, p: {xs: 2, md: 3}}}>
                    <Typography component="h1" variant="h4" align="center">
                        Watermelon Markets - Seller Survey
                    </Typography>

                    <Stepper activeStep={activeStep} sx={{pt: 3, pb: 5}}>
                        {steps.map((label) => (
                            <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                            </Step>
                        ))}
                    </Stepper>

                    {activeStep === steps.length ? (
                        <React.Fragment>
                            <Typography variant="h5" gutterBottom>
                                Thank you for your submission.
                            </Typography>
                        </React.Fragment>
                    ) : (
                        <React.Fragment>
                            <form onSubmit={onSubmit}>
                                {getStepContent(activeStep, handleFormChange, formData, error)}
                                <Box sx={{display: 'flex', justifyContent: 'flex-end'}}>
                                    {activeStep !== 0 && (
                                        <Button onClick={handleBack} sx={{mt: 3, ml: 1}}>
                                            Back
                                        </Button>
                                    )}

                                    <Button
                                        variant="contained"
                                        onClick={handleNext}
                                        sx={{mt: 3, ml: 1}}
                                    >
                                        {activeStep === (steps.length - 1) ? 'Submit' : 'Next'}
                                    </Button>
                                </Box>
                            </form>
                        </React.Fragment>
                    )}
                </Paper>
            </Container>
        </ThemeProvider>
    );
}