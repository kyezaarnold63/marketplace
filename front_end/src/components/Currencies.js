import * as React from 'react';
import {useState} from 'react';
import {Select, MenuItem} from '@mui/material';

const currencies = [
    {value: 'USD', label: 'US Dollar'},
    {value: 'EUR', label: 'Euro'},
    {value: 'JPY', label: 'Japanese Yen'},
    {value: 'GBP', label: 'British Pound'},
    {value: 'UGX', label: 'Ugandan Shilling'},
    // Add more currencies here
];

export default function CurrencySelect({handleFormChange, component}) {
    const [currency, setCurrency] = useState('USD');

    const handleChange = (event) => {
        setCurrency(event.target.value);

        handleFormChange(component, event.target.value);
    };

    return (
        <Select
            label=""
            value={currency}
            onChange={handleChange}
        >
            {currencies.map((option) => (
                <MenuItem key={option.value} value={option.value}>
                    {option.label}
                </MenuItem>
            ))}
        </Select>
    );
}
