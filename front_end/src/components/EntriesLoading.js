import * as React from 'react';


function EntriesLoading(Component) {
    return function EntryLoadingComponent({isLoading, ...props}) {
        if (!isLoading) return <Component {...props} />;
        return (
            <p style={{textAlign: 'center', fontSize: '30px'}}>
                Hold on, fetching data may take some time :)
            </p>
        );
    };
}

export default EntriesLoading;