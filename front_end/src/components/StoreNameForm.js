import * as React from 'react';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';

export default function StoreNameForm({handleFormChange, formData, error}) {

    const handleStoreNameFieldChange = (event) => {
        handleFormChange("store_name", event.target.value);
    }

    return (
        <React.Fragment>
            <Typography variant="h6" gutterBottom sx={{mb: '30px'}}>
                Store Name
            </Typography>
            <Typography variant="body1" component='p' sx={{textAlign: 'left', mb: '30px'}}>
                <span style={{fontWeight: '500', fontSize: '20px', lineHeight: '24px'}}>
                    What is the name of your store?
                </span><br/>
                Example: Razer Gold, Amazon, Target. (If you have multiple stores, choose your top gift card -- you can
                always add more later.)
            </Typography>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <TextField
                        error={error !== ""}
                        helperText={error}
                        fullWidth
                        variant="standard"
                        defaultValue={formData.store_name}
                        onChange={handleStoreNameFieldChange}
                    />
                </Grid>
            </Grid>
        </React.Fragment>
    );
}