import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import AppBar from "@mui/material/AppBar";
import * as React from "react";

export default function Header() {
    return (
        <React.Fragment>
            <AppBar
                position="absolute"
                color="default"
                elevation={0}
                sx={{
                    position: 'relative',
                    borderBottom: (t) => `1px solid ${t.palette.divider}`,
                }}
            >
                <Toolbar sx={{ backgroundColor: '#1A76D2', color: '#FFFFFF', border: 'none'}}>
                    <Typography variant="h6" color="inherit" noWrap>
                        Watermelon Markets Platform
                    </Typography>
                </Toolbar>
            </AppBar>
        </React.Fragment>
    )
}