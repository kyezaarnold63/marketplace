import * as React from 'react';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';

import Currencies from './Currencies';

export default function BalanceForm({ handleFormChange, formData, error }) {

    const handleBalanceFieldChange = (event) => {
        handleFormChange("balance_on_gift_card", event.target.value);
    }

    return (
        <React.Fragment>
            <Typography variant="h6" gutterBottom sx={{mb: '30px'}}>
                Balance on Gift Card
            </Typography>
            <Typography variant="body1" component='p' sx={{textAlign: 'left', mb: '30px'}}>
                <span style={{fontWeight: '500', fontSize: '20px', lineHeight: '24px'}}>
                    Balance:
                </span><br/>
                What is the balance left on your gift card? If possible, please double check your balance before
                entering the number here. Example answer: $100, 2,000 INR, 200 EUR.
            </Typography>
            <Grid container spacing={3}>
                <Grid item xs={12} sm={2}>
                    <TextField
                        error={error !== ""}
                        helperText={error}
                        fullWidth
                        variant="standard"
                        defaultValue={formData.balance_on_gift_card}
                        onChange={handleBalanceFieldChange}
                    />
                </Grid>
                <Grid item xs={12} sm={2}>
                    <Currencies handleFormChange={handleFormChange} component='balance_currency' />
                </Grid>
            </Grid>
        </React.Fragment>
    );
}