import * as React from 'react';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';

export default function WalletAddressForm({ handleFormChange, formData, error }) {

    const handleWalletAddressFieldChange = (event) => {
        handleFormChange("crypto_wallet_address", event.target.value);
    }

    return (
        <React.Fragment>
            <Typography variant="h6" gutterBottom sx={{mb: '30px'}}>
                Wallet Address
            </Typography>
            <Typography variant="body1" component='p' sx={{textAlign: 'left', mb: '30px'}}>
                <span style={{fontWeight: '500', fontSize: '20px', lineHeight: '24px'}}>
                    What Polygon or Ethereum address would you like to receive funds at?
                </span><br/>
                You may change this later. Example: 0xfA63Ca5caF1D88f42e1A73aE0E0cb7060B9E7292
            </Typography>
            <Grid container spacing={3}>
                <Grid item xs={12} sm={6}>
                    <TextField
                        error={error !== ""}
                        helperText={error}
                        fullWidth
                        variant="standard"
                        defaultValue={formData.crypto_wallet_address}
                        onChange={handleWalletAddressFieldChange}
                    />
                </Grid>
            </Grid>
        </React.Fragment>
    );
}