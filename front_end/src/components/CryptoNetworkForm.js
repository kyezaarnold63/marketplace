import * as React from 'react';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import {MenuItem, Select} from "@mui/material";
import {useState} from "react";
import * as net from "net";

const networks = [
    {value: 'polygon', label: 'Polygon'},
    {value: 'ethereum', label: 'Ethereum'},
];

export default function CryptoNetworkForm({ handleFormChange, formData, error }) {
    const [network, setNetwork] = useState('polygon');

    const handleChange = (event) => {
        setNetwork(event.target.value);
        handleFormChange("crypto_network", network);
    }

    return (
        <React.Fragment>
            <Typography variant="h6" gutterBottom sx={{mb: '30px'}}>
                Crypto Network
            </Typography>
            <Typography variant="body1" component='p' sx={{textAlign: 'left', mb: '30px'}}>
                <span style={{fontWeight: '500', fontSize: '20px', lineHeight: '24px'}}>
                    Which network would you like to receive funds at?
                </span><br/>
                If you are unsure, please select Polygon. (You may change this later.)
            </Typography>
            <Grid container spacing={3}>
                <Grid item xs={12} sm={4}>
                    <Select
                        defaultValue={formData.crypto_network}
                        fullWidth
                        onChange={handleChange}
                    >
                        {networks.map((option) => (
                            <MenuItem key={option.value} value={option.value}>
                                {option.label}
                            </MenuItem>
                        ))}
                    </Select>
                </Grid>
            </Grid>
        </React.Fragment>
    );
}