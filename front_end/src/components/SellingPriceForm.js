import * as React from 'react';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';

import Currencies from './Currencies';

export default function SellingPriceForm({ handleFormChange, formData, error }) {

    const handleSellingPriceFieldChange = (event) => {
        handleFormChange("card_selling_price", event.target.value);
    }

    return (
        <React.Fragment>
            <Typography variant="h6" gutterBottom sx={{mb: '30px'}}>
                Selling Price
            </Typography>
            <Typography variant="body1" component='p' sx={{textAlign: 'left', mb: '30px'}}>
                <span style={{fontWeight: '500', fontSize: '20px', lineHeight: '24px'}}>
                    What price are you selling at?
                </span><br/>
                Example Answer: $70. Please remember a low prices generally results in a faster and more
                successful sale, and we recommend a substantial discount. This number must be strictly at least ten
                percent less than the face value of the card, as we only sell discounted gift cards.
            </Typography>
            <Grid container spacing={3}>
                <Grid item xs={12} sm={2}>
                    <TextField
                        error={error !== ""}
                        helperText={error}
                        fullWidth
                        variant="standard"
                        defaultValue={formData.card_selling_price}
                        onChange={handleSellingPriceFieldChange}
                    />
                </Grid>
                <Grid item xs={12} sm={2}>
                    <Currencies handleFormChange={handleFormChange} component={'card_price_currency'}/>
                </Grid>
            </Grid>
        </React.Fragment>
    );
}