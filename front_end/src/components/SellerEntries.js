import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Title from './Title';

export default function SellerEntries({entries}) {

    if (!entries || entries.length === 0) return <p>No entries, sorry</p>;

    return (
        <React.Fragment>
            <Title>Seller Survey Entires</Title>
            <Table size="small">
                <TableHead>
                    <TableRow>
                        <TableCell sx={{fontWeight: 'bold'}}>ID</TableCell>
                        <TableCell sx={{fontWeight: 'bold'}}>Survey&nbsp;ID</TableCell>
                        <TableCell sx={{fontWeight: 'bold'}}>Store&nbsp;Name</TableCell>
                        <TableCell align="right" sx={{fontWeight: 'bold'}}>Gift&nbsp;Card&nbsp;Amount</TableCell>
                        <TableCell align="right" sx={{fontWeight: 'bold'}}>Gift&nbsp;Card&nbsp;Price</TableCell>
                        <TableCell sx={{fontWeight: 'bold'}}>Crypto&nbsp;Network</TableCell>
                        <TableCell sx={{fontWeight: 'bold'}}>Wallet&nbsp;Address</TableCell>
                        <TableCell sx={{fontWeight: 'bold'}}>Email</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {entries.map((entry) => (
                        <TableRow key={entry.id}>
                            <TableCell>{entry.id}</TableCell>
                            <TableCell>{entry.uuid}</TableCell>
                            <TableCell>{entry.store_name}</TableCell>
                            <TableCell
                                align="right">{`${entry.balance_on_gift_card} (${entry.balance_currency})`}</TableCell>

                            <TableCell
                                align="right">{`${entry.card_selling_price} (${entry.card_price_currency})`}</TableCell>
                            <TableCell>{entry.crypto_network}</TableCell>
                            <TableCell>{entry.crypto_wallet_address}</TableCell>
                            <TableCell>{entry.email}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </React.Fragment>
    );
}