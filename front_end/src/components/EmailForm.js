import * as React from 'react';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';

export default function EmailForm({handleFormChange, formData, error }) {

    const handleEmailFieldChange = (event) => {
        handleFormChange("email", event.target.value);
    }

    return (
        <React.Fragment>
            <Typography variant="h6" gutterBottom sx={{ mb: '30px'}}>
                Email
            </Typography>
            <Typography variant="body1" component='p' sx={{ textAlign: 'left', mb: '30px' }}>
                <span style={{ fontWeight: '500', fontSize: '20px', lineHeight: '24px' }}>
                    Almost done! What is your email address?
                </span><br/>
                We will only use this to reach out about the transaction. Please check this often as we'll use this as
                the main way of getting in touch.
            </Typography>
            <Grid container spacing={3}>
                <Grid item xs={12} sm={4}>
                    <TextField
                        error={error !== ""}
                        helperText={error}
                        fullWidth
                        variant="standard"
                        defaultValue={formData.email}
                        onChange={handleEmailFieldChange}
                    />
                </Grid>
            </Grid>
        </React.Fragment>
    );
}