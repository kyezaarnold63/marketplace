FROM python:3
LABEL authors="kyeza"

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

WORKDIR /back_end

COPY requirements.txt /back_end/
RUN /usr/local/bin/python -m pip install --upgrade pip
RUN pip install -r requirements.txt

COPY . /back_end/

EXPOSE 8000

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
